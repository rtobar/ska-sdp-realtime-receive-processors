from setuptools import find_namespace_packages, setup

with open("README.rst") as readme_file:
    readme = readme_file.read()
with open(".release") as release_file:
    for line in release_file:
        if "release" in line:
            version = line.removeprefix("release=").strip()
            break
with open("src/realtime/receive/processors/version.py", "w") as version_file:
    version_file.write(
        f"""
# This is an automatically-generated file
# DO NOT EDIT

_version = '{version}'
""".lstrip()
    )

setup(
    name="ska-sdp-realtime-receive-processors",
    version=version,
    description="Base infrastructure used by all plasma processor in the realtime workflow",
    long_description=readme,
    long_description_content_type="text/restructured-text",
    author="Stephen Ord, Rodrigo Tobar, Callan Gray",
    author_email="stephen.ord@csiro.au, rtobar@icrar.org, callan.gray@icrar.org",
    url="https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-processors",
    packages=find_namespace_packages(where='src'),
    package_dir={"": "src"},
    entry_points={
        "console_scripts": [
            "plasma-processor=realtime.receive.processors.main:main"
        ]
    },
    license="BSD license",
    zip_safe=True,
    test_suite="tests",
    install_requires=[
        "ska-sdp-dal >= 0.2.1",
        "ska-sdp-dal-schemas >= 0.3.2",
        "ska-sdp-realtime-receive-core >= 3.3.1",
        "ska-ser-logging",
    ],
)
