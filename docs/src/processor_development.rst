Writing a new Processor
=======================

To write a new processor you need to:

* Install this package.
* Create a new class that derives from
  :class:`realtime.receive.processors.base_processor.BaseProcessor`
  and implement the necessary methods.
* Launch ``plasma-processor <your-fully-classified-class-name> [args]``.

In particular,
when writing a class deriving from :class:`realtime.receive.processors.base_processor.BaseProcessor`
you'll be overwriting at most four methods (all of which are optional):

* :func:`realtime.receive.processors.base_processor.BaseProcessor.create`
  is a static method taking a list of strings.
  These are command line arguments,
  and allows your class be configured
  via any particular command-line flags you require.
  This method should return a new instance of your class.
* :func:`realtime.receive.processors.base_processor.BaseProcessor.process`
  is the main entry point of your class.
  It is invoked every time a
  :class:`payload <realtime.receive.processors.payload.PlasmaPayload>`
  is delivered to this processor.
  The payload should contain all relevant fields.
* :func:`realtime.receive.processors.base_processor.BaseProcessor.timeout`
  is invoked if no payloads have been received
  after a given amount of time.
  See :doc:`running` for details.
* :func:`realtime.receive.processors.base_processor.BaseProcessor.close`
  is invoked when the processor is being shut down.
