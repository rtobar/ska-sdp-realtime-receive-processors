.. ska-sdp-realtime-receive-processors documentation master file, created by
   sphinx-quickstart on Fri Jun 24 14:12:58 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SDP Receive Processors
======================

This package contains the shared code for the SDP receiver processors.

*Processors* are programs that connect to a Plasma store
and receive payloads from the receiver pipeline.
This package aims to ease the development of such programs,
dealing with all the complexity of talking to plasma,
unpacking payloads,
allowing developers to concentrate on their business logic.

To understand where do these processors live
in the context of the receive pipeline
read :doc:`context`.

To understand how to write your own processor
using this package
read :doc:`processor_development`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   context
   running
   processor_development
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
