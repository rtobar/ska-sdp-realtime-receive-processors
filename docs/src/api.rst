API documentation
=================

Processors
----------

.. autoclass:: realtime.receive.processors.base_processor.BaseProcessor
   :members:
.. autoclass:: realtime.receive.processors.mswriter.MSWriterProcessor

Others
------

.. autoclass:: realtime.receive.processors.payload.PlasmaPayload
.. autoclass:: realtime.receive.processors.main.QueuingProcessor
.. autoclass:: realtime.receive.processors.main.Runner
