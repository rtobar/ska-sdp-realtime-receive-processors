Running
=======

This package provides not only the base class for all plasma processors,
but also the program used to load and run them.
This program is called ``plasma-processor``.

``plasma-processor`` loads a user-provided processor class name
(its only required parameter)
and sets up the necessary infrastructure
to connect it to a Plasma store.
While most of the logic of the program
resides in these user-provided classes,
``plasma-processor`` provides some options to the user:

* The plasma socket which the program should connect to.
* The amount of time after which
  a "timeout" should be declared (in seconds).
  A timeout happens
  when no payload has been received
  after a given amount of time.
* Verbosity and help.

Run ``plasma-processor -h`` to see the details.
