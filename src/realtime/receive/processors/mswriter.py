import argparse
import asyncio
import concurrent.futures
import functools
import logging
from datetime import datetime
from pathlib import Path
from typing import Iterable, Optional

from realtime.receive.core import msutils

from realtime.receive.processors.command_executor import CommandExecutor

from .base_processor import BaseProcessor
from .payload import PlasmaPayload

logger = logging.getLogger(__name__)


class MSWriterProcessor(BaseProcessor):
    """A Processor that writes incoming payloads into a Measurement Set"""

    def __init__(
        self,
        output_ms: str,
        max_payloads: Optional[int] = None,
        max_ms: Optional[int] = None,
        use_plasma_ms: bool = False,
        command_template: Optional[Iterable[str]] = None,
        timestamp_output: bool = False,
    ):
        self.output_ms = output_ms
        self.received_payloads = 0
        self.process_pollrate = 1.0
        self.max_payloads = max_payloads
        self.max_ms = max_ms
        self.mswriter: Optional[msutils.MSWriter] = None
        self.ms_written = 0
        self._use_plasma_ms = use_plasma_ms
        self._timestamp_output = timestamp_output
        self._last_scan_id = None
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

        # Command execution is optional
        self._command_executor = None
        if command_template:
            self._command_executor = CommandExecutor(command_template)

    @staticmethod
    def create(argv) -> "MSWriterProcessor":
        parser = argparse.ArgumentParser(
            description="A processor that writes Measurement Sets",
            prog="MSWriterProcessor",
        )
        parser.add_argument(
            "output_ms", help="The Measurement Set to write the output to"
        )
        parser.add_argument(
            "--max-payloads",
            type=int,
            default=None,
            help="Sets the maximum number of payloads per measurement set",
        )
        parser.add_argument(
            "--max-ms",
            type=int,
            default=None,
            help="Sets the maximum number of Measurement Sets to write",
        )
        parser.add_argument(
            "--use-plasma-ms",
            action="store_true",
            help="Whether to uses plasma objects in the measurement set",
        )
        parser.add_argument(
            "--timestamp-output",
            action="store_true",
            help="Whether to generate timestamped ms local filenames",
        )
        parser.add_argument(
            "-c",
            "--ms-command",
            type=lambda s: s.split(" "),
            help="Command to execute on each new Measurement Set",
        )
        args = parser.parse_args(argv)
        return MSWriterProcessor(
            output_ms=args.output_ms,
            max_payloads=args.max_payloads,
            max_ms=args.max_ms,
            use_plasma_ms=args.use_plasma_ms,
            command_template=args.ms_command,
            timestamp_output=args.timestamp_output,
        )

    def _generate_output_path(self, scan_id) -> str:
        """
        Generates an output path using UTC DateTime formated extension
        from when the payload was received
        """
        p = Path(self.output_ms)
        if self._timestamp_output:
            # UTC Date Time Format
            return (
                f"{p.parent}/{p.stem}.scan-{scan_id}"
                f".{datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')}"
                f"{p.suffix}"
            )
        else:
            return f"{p.parent}/{p.stem}.scan-{scan_id}{p.suffix}"

    async def process(self, payload: PlasmaPayload):
        """
        Runs a plasma mswriter that receives plasma payloads and constructs
        plasma measurement sets.
        """

        # Write if the next scan is received
        if (
            self._last_scan_id is not None
            and self._last_scan_id != payload.scan.scan_number
            and self.mswriter
        ):
            logger.info("Next scan started")
            await self._finish_writing()
        self._last_scan_id = payload.scan.scan_number

        loop = asyncio.get_running_loop()
        if not self.mswriter:
            output_path = self._generate_output_path(payload.scan.scan_number)
            logger.info("Writing to %s", output_path)
            self.mswriter = await loop.run_in_executor(
                self.executor,
                functools.partial(
                    msutils.MSWriter,
                    output_path,
                    payload.scan,
                    payload.antennas,
                    payload.baselines,
                    plasma_socket=self._plasma_socket
                    if self._use_plasma_ms
                    else None,
                ),
            )
        vis_tensor = payload.visibilities
        vis_tensor_ref = msutils.TensorRef(
            vis_tensor.oid.binary(), vis_tensor.get()
        )
        # TODO (rtobar): determine which beam this payload corresponds to
        # (probably based on channel id?)
        beam = payload.scan.scan_type.beams[0]
        sw = beam.channels.spectral_windows[0]
        await loop.run_in_executor(
            self.executor,
            functools.partial(
                self.mswriter.write_payload,
                payload,
                beam,
                sw,
                payload.time_index,
                payload.uvw,
                payload.interval,
                payload.exposure,
                visibility_tensor_ref=vis_tensor_ref,
            ),
        )
        self.received_payloads += 1

        # Write if max payloads reached
        if (
            self.max_payloads is not None
            and self.received_payloads >= self.max_payloads
        ):
            logger.info("Max payloads received")
            await self._finish_writing()

        return self.ms_written == self.max_ms

    async def timeout(self):
        if self.received_payloads != 0:
            logger.info("Finishing MS writing due to timeout")
            await self._finish_writing()
        return self.ms_written == self.max_ms

    async def _finish_writing(self):
        assert self.mswriter is not None
        self.received_payloads = 0
        ms_filename = self.mswriter.ms.name
        asyncio.get_running_loop().run_in_executor(
            self.executor, self.mswriter.close
        )
        self.mswriter.close()
        self.mswriter = None
        logger.info("Finished writing %s", ms_filename)
        self.ms_written += 1
        if self._command_executor:
            self._command_executor.schedule(ms_filename)

    async def close(self):
        """Signal this processor to stop its activity and return"""
        if self.mswriter:
            await self._finish_writing()
        if self._command_executor:
            self._command_executor.stop()
