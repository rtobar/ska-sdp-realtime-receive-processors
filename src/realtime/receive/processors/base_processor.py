from typing import Iterable, Optional

from realtime.receive.processors.payload import PlasmaPayload


class BaseProcessor:
    """
    Base class for all Processors

    Subclasses should override the create, process, timeout and close methods
    as appropriate.
    """

    def __init__(self):
        """
        Creates a new BaseProcessor.
        """
        self._plasma_socket = "/tmp/plasma_socket"

    @staticmethod
    def create(argv: Iterable[str]) -> "BaseProcessor":
        """
        Creates an instance of this class from the given command line
        parameters. This allows user-provided classes to have their own command
        line parsing logic, and receive arbitrary user-provided parameters.

        :param argv: A list of command line parameters.
        :returns: A new instance of this class.
        """

    def set_plasma_socket(self, plasma_socket: str):
        """
        Sets the path to the socket through which communication with Plasma
        takes place. The processor should not usually need to communicate with
        Plasma, but it can if necessary.

        :param plasma_socket: The plasma socket used by this processor.
        """
        self._plasma_socket = plasma_socket

    async def process(self, payload: Optional[PlasmaPayload]) -> bool:
        """
        Processes the given payload.

        :param payload: A payload read from Plasma.
        :returns: Whether this processor should not be called anymore.
        """
        return False

    async def timeout(self) -> bool:
        """
        Indicates a timeout on receiving payloads has occurred.

        :returns: Whether this processor should not be called anymore.
        """
        return False

    async def close(self):
        """
        Stop any pending actions in this processor.
        """
        pass

    async def __aenter__(self):
        return self

    async def __aexit__(self, *args, **kwargs):
        await self.close()
