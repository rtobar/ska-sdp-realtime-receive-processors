import logging

import awkward as ak
from astropy import units
from astropy.coordinates import Angle
from realtime.receive.core.scan import (
    Beam,
    Channels,
    Field,
    PhaseDirection,
    Polarisations,
    Scan,
    ScanType,
    SpectralWindow,
    StokesType,
)

logger = logging.getLogger(__name__)


def plasma_to_scan(
    scan_id: int,
    scan_type_id: str,
    beams_table: ak.Array,
    pols_table: ak.Array,
    field_table: ak.Array,
    spectral_window_table: ak.Array,
    channels_table: ak.Array,
) -> Scan:
    """
    Converts plasma payload data to the scan model
    for use in plasma processing apps.
    """
    fields = {
        f["field_id"]: Field(
            f["field_id"],
            PhaseDirection(
                Angle(f["phase_dir"]["ra"], units.rad),
                Angle(f["phase_dir"]["dec"], units.rad),
                f["phase_dir"]["reference_time"],
                f["phase_dir"]["reference_frame"],
            ),
        )
        for f in field_table.to_list()
    }
    spectral_window = [
        SpectralWindow(
            sw["spectral_window_id"],
            sw["count"],
            sw["start"],
            sw["freq_min"],
            sw["freq_max"],
            sw["stride"],
        )
        for sw in spectral_window_table.to_list()
    ]
    channels = {
        c["channels_id"]: Channels(
            c["channels_id"],
            spectral_windows=[
                spectral_window[index]
                for index, sw in enumerate(spectral_window_table)
                if sw["channels_id"] == c["channels_id"]
            ],
        )
        for c in channels_table.to_list()
    }
    polarisations = {
        p["polarizations_id"]: Polarisations(
            p["polarizations_id"], [StokesType[s] for s in p["corr_type"]]
        )
        for p in pols_table.to_list()
    }
    beams = [
        Beam(
            b["beam_id"],
            b["function"],
            channels[b["channels_id"]],
            polarisations[b["polarizations_id"]],
            fields[b["field_id"]],
        )
        for b in beams_table.to_list()
    ]
    return Scan(scan_id, ScanType(scan_type_id, beams))
