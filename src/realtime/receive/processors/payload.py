from typing import Sequence

import ska_sdp_dal as rpc
from realtime.receive.core import icd
from realtime.receive.core.antenna import Antenna
from realtime.receive.core.baselines import Baselines
from realtime.receive.core.scan import Scan


class PlasmaPayload(icd.Payload):
    """
    A payload for the plasma communications that looks like the ICD payload.
    """

    def __init__(
        self,
        scan: Scan,
        times: rpc.TensorRef,
        channel_ids: rpc.TensorRef,
        vis: rpc.TensorRef,
        uvw: rpc.TensorRef,
        flag: rpc.TensorRef,
        weight: rpc.TensorRef,
        sigma: rpc.TensorRef,
        intervals: rpc.TensorRef,
        exposures: rpc.TensorRef,
        time_index: rpc.TensorRef,
        baselines: Baselines,
        antennas: Sequence[Antenna],
    ):
        super().__init__()

        channel_id = channel_ids.get()[0]
        chans_per_payload = len(channel_ids.get())

        self.scan_id = scan.scan_number
        self.baseline_count = len(baselines)
        self.channel_count = chans_per_payload
        self.timestamp_count, self.timestamp_fraction = icd.mjd_to_icd(
            times.get()[0]
        )
        self.visibilities = vis
        self.flags = flag
        self.weights = weight
        self.sigma = sigma
        self.channel_id = channel_id

        # Extra information read from Plasma and held in this payload
        self.scan = scan
        self.uvw = uvw.get()
        self.antennas = antennas
        self.baselines = baselines
        self.interval = intervals.get()[0]
        self.exposure = exposures.get()[0]
        self.time_index = time_index.get()[0]
