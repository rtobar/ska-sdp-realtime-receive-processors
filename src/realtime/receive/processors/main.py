import argparse
import asyncio
import collections
import concurrent.futures
import importlib
import logging
import signal
from typing import Deque, Optional, Union

import numpy as np
import ska_sdp_dal as rpc
import ska_ser_logging
from realtime.receive.core.antenna import Antenna
from realtime.receive.core.baselines import Baselines
from sdp_dal_schemas import PROCEDURES

from .base_processor import BaseProcessor
from .payload import PlasmaPayload
from .plasma_utils import plasma_to_scan

logger = logging.getLogger(__name__)


class QueuingProcessor(rpc.Processor):
    """
    A Processor that reacts to read_payload by storing its inputs into a
    PlasmaPayload and PlasmaTM, and putting it into a deque, which can be later
    read from.
    """

    def __init__(self, *args, **kwargs):
        super().__init__([PROCEDURES["read_payload"]], *args, **kwargs)
        self.bytes_received = 0
        self.payloads: Deque[PlasmaPayload] = collections.deque()

    def read_payload(
        self,
        scan_id,
        time_index,
        times,
        intervals,
        exposures,
        channel_ids,
        flag,
        weight,
        sigma,
        beam,
        baselines,
        spectral_window,
        channels,
        antennas,
        field,
        polarizations,
        uvw,
        vis,
        output,
    ):
        """Reads the payload from parameters into this object's members"""

        # Build a Scan, Baselines and Antenna objects from the tables in Plasma
        #
        # TODO (rtobar): most of this information is common either globally
        # through the observation or per scan type. We could avoid re-creating
        # most of these objects if we cache them, as their plasma counterparts
        # should also present throughout the life of the observation.

        scan = plasma_to_scan(
            int(scan_id.get()[0]),
            # TODO: duplicated as scan_type_id is never used by mswriter
            str(scan_id.get()[0]),
            beam.get_awkward(),
            polarizations.get_awkward(),
            field.get_awkward(),
            spectral_window.get_awkward(),
            channels.get_awkward(),
        )
        antennas = [
            Antenna(
                antenna["name"],
                antenna["dish_diameter"],
                antenna["position"]["x"],
                antenna["position"]["y"],
                antenna["position"]["z"],
            )
            for antenna in antennas.get_awkward()
        ]
        baselines = baselines.get_awkward()
        baselines = Baselines(baselines["antenna1"], baselines["antenna2"])

        payload = PlasmaPayload(
            scan,
            times,
            channel_ids,
            vis,
            uvw,
            flag,
            weight,
            sigma,
            intervals,
            exposures,
            time_index,
            baselines,
            antennas,
        )

        self.payloads.append(payload)
        output.put(np.empty(0, dtype="i1"))

    def pop_payload(self) -> Optional[PlasmaPayload]:
        """Detaches a received payload and returns it to the caller"""
        if self.payloads:
            return self.payloads.popleft()
        return None


def _load_class(user_processor_class: Union[BaseProcessor, str]):
    if isinstance(user_processor_class, type):
        return user_processor_class
    module, _, classname = user_processor_class.rpartition(".")
    return getattr(importlib.import_module(module), classname)


class Runner:
    """
    Runs a QueueingProcessor asynchronously, passing its payloads to a
    user-provided processing object.
    """

    def __init__(
        self,
        plasma_socket: str,
        user_processor: BaseProcessor,
        payload_timeout: Optional[int] = None,
        readiness_file: Optional[str] = None,
    ):
        user_processor.set_plasma_socket(plasma_socket)
        self.queuing_processor = QueuingProcessor(plasma_socket)
        self.user_processor: BaseProcessor = user_processor
        self.active = True
        self.received_payloads = 0
        self.last_payload = None
        self.process_pollrate = 1.0
        self.payload_timeout = payload_timeout
        self._readiness_file = readiness_file

    def _mark_as_ready(self):
        if self._readiness_file:
            with open(self._readiness_file, "wb"):
                pass

    async def run(self):
        """
        Runs the main loop, reacting to payloads being sent via RPC calls,
        and passing them to the user-provided processors.
        """
        logger.info("Started processor runner, waiting for incoming calls")
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        loop = asyncio.get_event_loop()
        time_since_last_payload = 0

        async with self.user_processor as user_processor:
            self._mark_as_ready()
            while self.active:
                # Run the plasma RPC processor and check if a payload was
                # yielded, passing it to the user's processor
                await loop.run_in_executor(
                    executor,
                    self.queuing_processor.process,
                    self.process_pollrate,
                )
                payload = self.queuing_processor.pop_payload()
                if payload:
                    time_since_last_payload = 0
                    self.last_payload = payload
                    try:
                        if await user_processor.process(payload):
                            break
                    except Exception:
                        logger.exception("Error while processing payload")
                else:
                    time_since_last_payload += self.process_pollrate
                    if (
                        self.payload_timeout is not None
                        and time_since_last_payload >= self.payload_timeout
                    ):
                        logger.info(
                            "No payload received after %.3f [s]",
                            self.payload_timeout,
                        )
                        try:
                            if await user_processor.timeout():
                                break
                        except Exception:
                            logger.exception("Error while processing timeout")

    async def close(self):
        """Signal this runner to stop its activity and return"""
        self.active = False


def main():

    parser = argparse.ArgumentParser(
        description="Runs an user-provided receive processor"
    )
    parser.add_argument(
        "user_processor_class", help="The class implementing the Processor"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="If set, more verbose output will be produced",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--plasma_socket",
        help="The socket where Plasma is listening for connections",
        default="/tmp/plasma",
    )
    parser.add_argument(
        "-r",
        "--readiness-file",
        help=(
            "An empty file that will be created after the processor has "
            "finished setting up, signaling it's ready to receive data"
        ),
    )
    parser.add_argument(
        "--payload_timeout",
        type=int,
        default=None,
        help="Sets the maximum number of payload retries",
    )

    args, user_processor_args = parser.parse_known_args()

    logging_level = logging.DEBUG if args.verbose else logging.INFO
    ska_ser_logging.configure_logging(level=logging_level)

    user_processor = _load_class(args.user_processor_class).create(
        user_processor_args
    )
    runner = Runner(
        plasma_socket=args.plasma_socket,
        user_processor=user_processor,
        payload_timeout=args.payload_timeout,
        readiness_file=args.readiness_file,
    )

    loop = asyncio.get_event_loop()

    def stop_runner(_sig, _stack):
        logger.info("Signalling runner to stop")
        loop.create_task(runner.close())

    signal.signal(signal.SIGTERM, stop_runner)
    signal.signal(signal.SIGINT, stop_runner)

    loop.run_until_complete(runner.run())


if __name__ == "__main__":
    main()
