# -*- coding: utf-8 -*-


import logging
import queue
import subprocess as sp
import threading
from typing import Iterable

logger = logging.getLogger(__name__)


def _replace_fname(arg: str, filename: str):
    if "%s" in arg:
        return arg % filename
    return arg


class CommandExecutor:
    """
    Container of an infinite thread that executes a command for each given
    filename based on a template command line. Commands are executed on a
    separate thread, one at a time.
    """

    def __init__(self, command_template: Iterable[str]):
        self._command_template = command_template
        self._to_process = queue.Queue()
        self._launcher_thread = threading.Thread(target=self._run_commands)
        self._launcher_thread.start()

    def __del__(self):
        self.stop()

    def schedule(self, filename):
        """
        Schedule an execution of the underlying command for the given file
        """
        self._to_process.put_nowait(filename)

    def _run_commands(self):
        while True:
            # stop() puts a None, signalling the end of the loop
            ms_filename = self._to_process.get()
            if not ms_filename:
                break
            cmd = [
                _replace_fname(arg, ms_filename)
                for arg in self._command_template
            ]
            cmdline = sp.list2cmdline(cmd)
            logger.info("Launching: %s", cmdline)
            use_shell = True
            if use_shell:
                # supports shell commands e.g. &&, ||, >
                result = sp.run(
                    ["bash", "-c", cmdline], stdout=sp.PIPE, stderr=sp.PIPE
                )
            else:
                result = sp.run(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
            msg = 'Command "%s" exited with status %d'
            args = [cmdline, result.returncode]
            if not result.stdout == b"":
                msg += ".\n[stdout]\n%s"
                args += [result.stdout.decode().rstrip()]
            if not result.stderr == b"":
                msg += ".\n[stderr]\n%s"
                args += [result.stderr.decode().rstrip()]
            if result.returncode == 0:
                logger.info(msg, *args)
            else:
                logger.error(msg, *args)

    def stop(self):
        """Awaits any background processing commands."""
        self._to_process.put_nowait(None)
        self._launcher_thread.join(timeout=30)
        if self._launcher_thread.is_alive():
            logger.warning("Command launcher thread didn't finish after 30s")
